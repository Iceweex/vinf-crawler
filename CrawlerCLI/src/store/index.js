import Vue from 'vue'
import Vuex from 'vuex'

import example from './module-example'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      example
    },
    state: {
      posts_count: null,
      posts: [],
      tags:[],
      highlight: false,
      chart_data: {
        labels: [],
        datasets: [
          {
            label: 'Tags Frequency',
            backgroundColor: '#f87979',
            data: []
          }
        ]
        }
    },
    mutations: {
      update (state, data) {
        // mutate state
        state.posts = data;
      },
      addTag (state, data) {
        // mutate state
        data.map( a => {
          state.tags.push(a.key)
        });
      },
      updateHighlight (state, data) {
        // mutate state
        state.highlight = data;
      },
      updateCount (state, data) {
        // mutate state
        state.posts_count = data;
      },
      updateChart(state, data) {
        // mutate state
        state.chart_data = data;
      }
    }
  })

  return Store
}
