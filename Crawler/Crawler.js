var request = require('request');
var cheerio = require('cheerio');
var fs = require('fs');
var steem = require('steem');

var pagesToVisit = [];
var maximum = 10000;

function getSteemitPosts(limit, postCounter, permlink, author){
    var limitCounter;

    if(limit <= 99){
        limitCounter = limit;
        limit = 0;
    }else{
        limitCounter = 99;
        limit = limit - 99;
    }

    steem.api.getDiscussionsByCreated({
        limit: limitCounter,
        tag: 'life',
        start_author: author,
        start_permlink: permlink
      }, function(err, result) {
            if(err === null){
                var i, len = result.length;
                for (i = 0; i < len; i++) {
                    var discussion = result[i];
                    var pageToVisit = "https://steemd.com" + discussion.url;
                    var url = "https://steemd.com" + discussion.url;
                    var page = {
                        'url' : url,
                        'permlink' : discussion.permlink,
                        'author' : discussion.author
                    };
                    pagesToVisit.push(page);
                    console.log(postCounter++, pageToVisit);
                    
                    if (i == len - 1) {
                        permlink = discussion.permlink;
                        author = discussion.author;
                        if(limit > 0){
                            getSteemitPosts(limit, postCounter, permlink, author);
                        }else{
                            visitPage(pagesToVisit.pop(), 0);
                        }
                    }

                }
            }else{
                console.log(err);
            }
      });   
};


getSteemitPosts(maximum,1, 'all-types-of-guns', 'owayneforrest');

function visitPage(page, post_id){
    request(page.url, function(error, response, html) {
        if(error) {
          console.log("Error: " + error);
        }
        // Check status code (200 is HTTP OK)
        console.log("Status code: " + response.statusCode);
        if(response.statusCode === 200) {
          // Parse the document body
          var $ = cheerio.load(html);
        //    console.log("Page title:  " + $('.post-title').text());
        //    console.log("Author:  " + $('div.post-head-bar > div > a').text());
        //   console.log("Payot:  " + $('.post-payout').text());
        //   console.log("Post category:  " + $('.post-category a').text());
        //    console.log("Body:  " + $('div.md > div > pre').text());
        //  console.log("Votes:  " +      $('.post-votes a').length);

         var postDetails = {
            'name': $('.post-title').text(),
            'author': $('div.post-head-bar > div > a').first().text().replace('@',''),
            'payout': $('.post-payout').first().text().replace('$',''),
            'date': $('.timeago2').attr("title"),
            'tag': $('.post-category a').text(),
            'body': $('div.md > div > pre').first().text(),
            'votes': $('.post-votes a').length
        };

        var id = { 'index': {"_id" : post_id}};

 
        fs.appendFile('output10k3.json', JSON.stringify(id) + '\n' + JSON.stringify(postDetails)  + '\n', function(err){

            // console.log('File successfully written! - Check your project directory for the output.json file');
        
        })

          var nextPage = pagesToVisit.pop();
          if(nextPage){
            visitPage(nextPage, ++post_id);
          }
        }else{
            // pagesToVisit = [];
            // getSteemitPosts(maximum - post_id, post_id + 1 , page.permlink, page.author);
            console.log('ERROR SKIP THIS PAGE');
            var nextPage = pagesToVisit.pop();
            visitPage(nextPage, ++post_id);
        }

     });
}

