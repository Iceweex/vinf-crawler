const express = require('express')
const elasticsearch = require('elasticsearch')
const app = express()
const port = 3000

var client = new elasticsearch.Client({  
  log: 'trace'
});

app.get('/', (req, res) => res.send('Hello World!'))

app.get('/es/total', function(req, res) {
  client.search({
      index: 'posts',
      type: 'post',
      body: {
        "from" : 0,
        "size" : 10000,
          'query': {
              'match_all': {}
          },
          "aggs" : {
            "posts" : {
                "terms" : {
                    "field" : "tag.keyword",
                    "size" : 5
                }
            }
        },
          
      }
  }).then(function (esResponse) {
      res.send({
          'total': esResponse.hits,
          'aggs': esResponse.aggregations
      });
  }, function (esError) { 
      console.error(esError);
  });
})

app.get('/es/morelike', function(req, res) {
    client.search({
        index: 'posts',
        type: 'post',
        body: {
          "from" : 0,
          "size" : 10000,
            'query': {
                "more_like_this" : {
                    // "fields" : ["name", "description"],
                    "like" : [
                    {
                        "_index" : "posts",
                        "_type" : "post",
                        "_id" : req.query.id
                    }
                    ],
                    "min_term_freq" : 1,
                    "max_query_terms" : 12
                }
            },
            "aggs" : {
                "posts" : {
                    "terms" : {
                        "field" : "tag.keyword",
                        "size" : 5
                    }
                }
            }
          }
    }).then(function (esResponse) {
        res.send({
            'total': esResponse.hits,
            'aggs': esResponse.aggregations
        });
    }, function (esError) { 
        console.error(esError);
    });
  })

app.get('/es/fulltext', function(req, res) {
    console.log(req);
    client.search({
        index: 'posts',
        type: 'post',
        body: {
          "from" : 0,
          "size" : 10000,
            'query': {

                "bool": {
                    "must": [
                        {
                            "bool": {
                                "should": [
                                    {
                                        "match": {
                                            "name" : {
                                                "query": req.query.text,
                                                "boost": 2,
                                                "analyzer": "standard",
                                                'operator': 'and'
                                            }
                                        }
                                    },
                                        {
                                            "match": {
                                                "body" : {
                                                    "query": req.query.text,
                                                    "analyzer": "standard",
                                                    'operator': 'and'
                                            }
                                        }
                                    }
                                ],
                                "minimum_should_match": 1
                            }

                        },
                        {
                            "terms" : {
                                "tag" : req.query.tags
                            }
                        }
                    ],
                     'filter' : [
                            {
                                'range' : {
                                    'payout' : {
                                        "gte" : req.query.payout_min,
                                        "lte" : req.query.payout_max
                                    }
                                }
                            },
                            {
                                "range" : {
                                    "date" : {
                                        "gte": req.query.date_min,
                                        "lte": req.query.date_max,
                                        "time_zone": "+01:00"
                                    }
                                }
                            }
                        ]
                   
                }
            },
            "aggs" : {
                "posts" : {
                    "terms" : {
                        "field" : "tag.keyword",
                        "size" : 5
                    }
                }
            },
            "highlight" : {
                "pre_tags" : ["<tag1>"],
                "post_tags" : ["</tag1>"],
                "fields" : [
                    {"name" : {}},
                    {"body" : {}}
                ]
                    
            }
        }
    }).then(function (esResponse) {
        res.send({
            'total': esResponse.hits,
            'aggs': esResponse.aggregations
        });
    }, function (esError) { 
        console.error(esError);
    });
  })

  app.get('/es/autocomplete', function(req, res) {
    console.log(req);
    client.search({
        index: 'posts',
        type: 'post',
        body: {
          "from" : 0,
          "size" : 5,
          "_source": "name",
          "query": {
            "match": {
                "name": {
                    "query":  req.query.text,
                    "analyzer": "standard" 
                }
            }
        }
        }
    }).then(function (esResponse) {
        res.send({
            'total': esResponse.hits
        });
    }, function (esError) { 
        console.error(esError);
    });
  })

app.listen(port, () => console.log(`Example app listening on port ${port}!`))

